const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    theme: {
      extend: {
        colors: {
          primary: '#319795',
          secondary: '#81E6D9',
          tertiary: '#EBF4FF',
        },
        fontFamily: {
            sans: [
                'Lato',
                ...defaultTheme.fontFamily.sans,
            ]
        },
      }
    }
}